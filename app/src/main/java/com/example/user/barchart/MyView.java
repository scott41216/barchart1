package com.example.user.barchart;

/**
 * Created by User on 2016/9/17.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class MyView extends View {
    public MyView(Context context){
        super(context);
    }
    public MyView(Context context,AttributeSet attr) {
        super(context,attr);
    }
    private Paint myPaint;
    private static final String myString1 = "Test for Bar Chart";
    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        myPaint = new Paint();

        //繪製標題
        myPaint.setColor(Color.BLACK); //設置畫筆顏色
        myPaint.setTextSize(18);//設置文字大小
        canvas.drawText(myString1, 20, 20, myPaint);

        //繪製坐標軸
        canvas.drawLine(50, 100, 50, 500, myPaint);//縱座標軸
        canvas.drawLine(50, 500, 400, 500, myPaint);//横座標轴
        int[] array1 = new int[]{0, 50, 100, 150, 200, 250, 300, 350};

        //繪製縱座標刻度
        myPaint.setTextSize(10);//設置文字大小
        canvas.drawText("單位 : 訊號強度", 20, 90, myPaint);
        for (int i = 0; i < array1.length; i++) {
            canvas.drawLine(50, 500 - array1[i], 54, 500 - array1[i], myPaint);
            canvas.drawText(array1[i] + "", 20, 500 - array1[i], myPaint);
        }

        //繪製橫坐標文字
        String[] array2 = new String[]{"2008年", "2009年", "2010年", "2011上半年"};
        for (int i = 0; i < array2.length; i++) {
            canvas.drawText(array2[i], array1[i] + 80, 520, myPaint);
        }

        //繪製條形圖形
        myPaint.setColor(Color.BLUE); //設置畫筆顏色
        myPaint.setStyle(Style.FILL); //設置填充
        canvas.drawRect(new Rect(90, 500 - 56, 110, 500), myPaint);//第一个矩形
        //劃一個矩形,前兩參數為左上座標，後倆為右下
        canvas.drawRect(new Rect(140, 500 - 98, 160, 500), myPaint);//第二个矩形
        canvas.drawRect(new Rect(190, 500 - 207, 210, 500), myPaint);//第三个矩形
        canvas.drawRect(new Rect(240, 500 - 318, 260, 500), myPaint);//第四个矩形
        myPaint.setColor(Color.BLACK); //设置画笔颜色
        canvas.drawText("56.32", 88, 500 - 58, myPaint);//第一个矩形的数字说明
        canvas.drawText("98.00", 138, 500 - 100, myPaint);//第二个矩形的数字说明
        canvas.drawText("207.65", 188, 500 - 209, myPaint);//第三个矩形的数字说明
        canvas.drawText("318.30", 238, 500 - 320, myPaint);//第四个矩形的数字说明
    }


}
